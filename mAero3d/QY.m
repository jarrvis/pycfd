
function q_y = QY(T, k, DY, call_case)


[JMAX, IMAX] = size(T);
dT_dy = zeros(JMAX, IMAX);

if (strcmp(call_case, 'Predict_F'))
    for i = 1:IMAX
        for j = 2:JMAX
            dT_dy(j,i) = (T(j,i) - T(j-1,i))/DY; 
        end
    end
    dT_dy(1,:) = (T(2,:) - T(1,:))/DY; 
elseif (strcmp(call_case, 'Correct_F'))
    for i = 1:IMAX
        for j = 1:JMAX-1
            dT_dy(j,i) = (T(j+1,i) - T(j,i))/DY;
        end
    end
    dT_dy(JMAX,:) = (T(JMAX,:) - T(JMAX-1,:))/DY;
else
    error('Undefined call case.')
end

q_y = - k.*dT_dy;