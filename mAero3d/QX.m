
function q_x = QX(T, k, DX, call_case)

[JMAX, IMAX] = size(T);
dT_dx = zeros(JMAX, IMAX);

if (strcmp(call_case, 'Predict_E'))
    for i = 2:IMAX
        for j = 1:JMAX
            dT_dx(j,i) = (T(j,i) - T(j,i-1))/DX; 
        end
    end
    dT_dx(:,1) = (T(:,2) - T(:,1))/DX; 
elseif (strcmp(call_case, 'Correct_E'))
    for i = 1:IMAX-1
        for j = 1:JMAX
            dT_dx(j,i) = (T(j,i+1) - T(j,i))/DX; 
        end
    end
    dT_dx(:,IMAX) = (T(:,IMAX) - T(:,IMAX-1))/DX; 
else
    error('Undefined call case.')
end
q_x = - k.*dT_dx;