function [E1, E2, E3, E5] = Primitive2E(rho, u, p, v, T, mu, lambda, k, c_v, DX, DY, call_case)


E1 = rho.*u;
tau_xx = TAUXX(u, v, lambda, mu, DX, DY, call_case);

E2 = rho.*u.^2 + p - tau_xx; % From x-momentum

tau_xy = TAUXY(u, v, mu, DX, DY, call_case);
E3 = rho.*u.*v - tau_xy; % From y-momentum

q_x = QX(T, k, DX, call_case);
E5 = (rho.*(c_v*T + (u.^2 + v.^2)/2) + p).*u - u.*tau_xx - v.*tau_xy + q_x;