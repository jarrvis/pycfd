
function mu = DYNVIS(T, mu_0, T_0)

mu = mu_0*(T/T_0).^(3/2)*(T_0 + 110)./(T + 110);