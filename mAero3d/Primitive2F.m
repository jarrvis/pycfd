
function [F1, F2, F3, F5] = Primitive2F(rho, u, p, v, T, mu, lambda, k, c_v, DX, DY, call_case)

F1 = rho.*v; 

tau_yx = TAUXY(u, v, mu, DX, DY, call_case); 
F2 = rho.*u.*v - tau_yx;


tau_yy = TAUYY(u, v, lambda, mu, DX, DY, call_case);

F3 = rho.*v.^2 + p - tau_yy;

q_y = QY(T, k, DY, call_case);
F5 = (rho.*(c_v*T + (u.^2 + v.^2)/2) + p).*v - u.*tau_yx - v.*tau_yy + q_y;