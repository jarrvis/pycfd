%%%%%%%%%%%%%
% Grid size %
%%%%%%%%%%%%%

IMAX = 70; % Number of grid points along the x-coordinate
JMAX = 70; % Number of grid points along the y-coordinate
MAXIT = 1e4; % Maximum number of time steps allowed before stopping the program
t = 1; % Time step counter
time = 0; % Physical time [s]

    
M_inf = 4;
a_inf = 340.28;
p_inf = 101325;
T_inf = 288.16; 
LHORI = 1e-5; 

T_w_T_inf = 1; 

gamma = 1.4;
mu_0 = 1.7894e-5;
T_0 = 288.16; 
Pr = 0.71; 
R = 287; 

c_v = R/(gamma - 1); 
c_p = gamma*c_v; 
rho_inf = p_inf/(R*T_inf);
Re_L = rho_inf*M_inf*a_inf*LHORI/DYNVIS(T_inf, mu_0, T_0); 
% e_inf = c_v*T_inf; % Freestream specific internal energy [J/kg]

delta = 5*LHORI/sqrt(Re_L); 

LVERT = 5*delta; 

DX = LHORI/(IMAX - 1); 
DY = LVERT/(JMAX - 1); 
x = 0:DX:LHORI; 
y = 0:DY:LVERT;

CFL = 0.6; 

p = ones(JMAX,IMAX)*p_inf;
rho = ones(JMAX,IMAX)*rho_inf;
T = ones(JMAX,IMAX)*T_inf;
T(1,:) = T_w_T_inf*T_inf; 
u = ones(JMAX,IMAX)*M_inf*a_inf;
u(1,:) = 0; 
v = zeros(JMAX,IMAX);
mu = DYNVIS(T, mu_0, T_0);
lambda = - 2/3*mu; 
k = THERMC(Pr, c_p, mu);

U1_p = zeros(JMAX, IMAX);
U2_p = zeros(JMAX, IMAX);
U3_p = zeros(JMAX, IMAX);
U5_p = zeros(JMAX, IMAX);
rho_p = zeros(JMAX, IMAX);
u_p = zeros(JMAX, IMAX);
v_p = zeros(JMAX, IMAX);
T_p = zeros(JMAX, IMAX);
p_p = zeros(JMAX, IMAX);


converged = false;
rho_old = rho;
while (~converged && t <= MAXIT)
    
    v_prime = max(max(4/3*mu(2:JMAX-1,2:IMAX-1).^2*gamma./(Pr*rho(2:JMAX-1,2:IMAX-1))));
    delta_t_CFL = 1./(abs(u(2:JMAX-1,2:IMAX-1))/DX + abs(v(2:JMAX-1,2:IMAX-1))/DY + ...
        sqrt(gamma*R*T(2:JMAX-1,2:IMAX-1))*sqrt(1/DX^2 + 1/DY^2) + 2*v_prime*(1/DX^2 + 1/DY^2));
    delta_t(t) = CFL*min(min(delta_t_CFL));
    
  
    U1 = rho; % From continuity
    U2 = rho.*u; % From momentum-x
    U3 = rho.*v; % From momentum-y
    U5 = rho.*(c_v*T + (u.^2 + v.^2)/2); % From energy
  
    [E1, E2, E3, E5] = Primitive2E(rho, u, p, v, T, mu, lambda, k, c_v, DX, DY, 'Predict_E');
    [F1, F2, F3, F5] = Primitive2F(rho, u, p, v, T, mu, lambda, k, c_v, DX, DY, 'Predict_F');
    
 
    for i = 2:IMAX-1
        for j = 2:JMAX-1
            U1_p(j,i) = U1(j,i) - delta_t(t)*((E1(j,i+1) - E1(j,i))/DX + (F1(j+1,i) - F1(j,i))/DY);
            U2_p(j,i) = U2(j,i) - delta_t(t)*((E2(j,i+1) - E2(j,i))/DX + (F2(j+1,i) - F2(j,i))/DY);
            U3_p(j,i) = U3(j,i) - delta_t(t)*((E3(j,i+1) - E3(j,i))/DX + (F3(j+1,i) - F3(j,i))/DY);
            U5_p(j,i) = U5(j,i) - delta_t(t)*((E5(j,i+1) - E5(j,i))/DX + (F5(j+1,i) - F5(j,i))/DY);
        end
    end
    
 
    [rho_p(2:JMAX-1,2:IMAX-1), u_p(2:JMAX-1,2:IMAX-1), v_p(2:JMAX-1,2:IMAX-1), T_p(2:JMAX-1,2:IMAX-1)] = U2Primitive(U1_p(2:JMAX-1,2:IMAX-1), ...
        U2_p(2:JMAX-1,2:IMAX-1), U3_p(2:JMAX-1,2:IMAX-1), U5_p(2:JMAX-1,2:IMAX-1), c_v);
    p_p(2:JMAX-1,2:IMAX-1) = rho_p(2:JMAX-1,2:IMAX-1)*R.*T_p(2:JMAX-1,2:IMAX-1);

    [rho_p, u_p, v_p, p_p, T_p] = BC(rho_p, u_p, v_p, p_p, T_p, rho_inf, M_inf*a_inf, p_inf, T_inf, T_w_T_inf, R, false);
    
    mu_p = DYNVIS(T_p, mu_0, T_0);
    lambda_p = - 2/3*mu_p;
    k_p = THERMC(Pr, c_p, mu_p);
    
    
    [E1_p, E2_p, E3_p, E5_p] = Primitive2E(rho_p, u_p, p_p, v_p, T_p, mu_p, lambda_p, k_p, c_v, DX, DY, 'Correct_E');
    [F1_p, F2_p, F3_p, F5_p] = Primitive2F(rho_p, u_p, p_p, v_p, T_p, mu_p, lambda_p, k_p, c_v, DX, DY, 'Correct_F');
    for i = 2:IMAX-1
        for j = 2:JMAX-1
            U1(j,i) = 1/2*(U1(j,i) + U1_p(j,i) - delta_t(t)*((E1_p(j,i) - E1_p(j,i-1))/DX + (F1_p(j,i) - F1_p(j-1,i))/DY));
            U2(j,i) = 1/2*(U2(j,i) + U2_p(j,i) - delta_t(t)*((E2_p(j,i) - E2_p(j,i-1))/DX + (F2_p(j,i) - F2_p(j-1,i))/DY));
            U3(j,i) = 1/2*(U3(j,i) + U3_p(j,i) - delta_t(t)*((E3_p(j,i) - E3_p(j,i-1))/DX + (F3_p(j,i) - F3_p(j-1,i))/DY));
            U5(j,i) = 1/2*(U5(j,i) + U5_p(j,i) - delta_t(t)*((E5_p(j,i) - E5_p(j,i-1))/DX + (F5_p(j,i) - F5_p(j-1,i))/DY));
        end
    end
    [rho(2:JMAX-1,2:IMAX-1), u(2:JMAX-1,2:IMAX-1), v(2:JMAX-1,2:IMAX-1), T(2:JMAX-1,2:IMAX-1)] = U2Primitive(U1(2:JMAX-1,2:IMAX-1), ...
        U2(2:JMAX-1,2:IMAX-1), U3(2:JMAX-1,2:IMAX-1), U5(2:JMAX-1,2:IMAX-1), c_v);
    p(2:JMAX-1,2:IMAX-1) = rho(2:JMAX-1,2:IMAX-1)*R.*T(2:JMAX-1,2:IMAX-1);

    [rho, u, v, p, T] = BC(rho, u, v, p, T, rho_inf, M_inf*a_inf, p_inf, T_inf, T_w_T_inf, R, false);
    
    mu = DYNVIS(T, mu_0, T_0);
    lambda = - 2/3*mu;
    k = THERMC(Pr, c_p, mu);

    converged = CONVER(rho_old, rho);
    rho_old = rho;
    
    time = time + delta_t(t);
    t = t + 1
end

if (converged)
    continuity = MDOT(rho, u, rho_inf, M_inf*a_inf, LVERT, DY); % If convergence has been achieved, check the validity of the solution by confirming conservation of mass
else
    error('CALCULATION FAILED: the maximum number of iterations has been reached before achieving convergence.')
end

if (continuity)
    disp('The calculation for M = 4 with a constant-temperature wall boundary condition has finished successfully.')
else
    error('The solution has converged to an invalid result.')
end

M = sqrt(u.^2 + v.^2)./sqrt(gamma*R*T);
figure
surf(x,y,M)
view(2)
axis equal
colorbar
title('Mach number (constant-temperature wall)')
xlabel('x')
ylabel('y')