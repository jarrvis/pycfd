

function k = THERMC(Pr, c_p, mu)

k = mu*c_p/Pr;