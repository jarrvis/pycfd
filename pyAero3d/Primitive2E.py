from TAUXX import TAUXX
from TAUXY import TAUXY
from QX import QX
import numpy as np


def Primitive2E(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,call_case):

# Calculate the flux vector E from the primitive flow field variables.
    E1=np.multiply(rho,u)
# To compute E2 first we need to calculate the x-direction of the normal stress (tau_xx)
    tau_xx=TAUXX(u,v,lambda_,mu,DX,DY,call_case)
    E2=np.multiply(rho,u ** 2) + p - tau_xx
# For E3 we need the xy component of the shear stress (tau_xy)
    tau_xy=TAUXY(u,v,mu,DX,DY,call_case)
    E3=np.multiply(np.multiply(rho,u),v) - tau_xy
# Finally, for E5 we also need the x-component of the heat flux (q_x)
    q_x=QX(T,k,DX,call_case)
    E5=(rho*(c_v*T + (u**2 + v**2)/2) + p)*u - u*tau_xx - v*tau_xy + q_x

    return E1,E2,E3,E5