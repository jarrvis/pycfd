
import numpy as np

def TAUYY(u,v,lambda_,mu,DX,DY,call_case):

# Calculate the y-direction of the normal stress.
    
    JMAX,IMAX=v.shape
    du_dx=np.zeros((JMAX,IMAX))
    dv_dy=np.zeros((JMAX,IMAX))

# Calculate the x derivative of u and the y derivative of v (divergence of 2D velocity)
# Forward difference when i,j = 1 and a rearward difference when i,j = IMAX,JMAX.
# To compute dv_dy:
    if (call_case=='Predict_F'):
        for i in np.arange(0,IMAX):
            for j in np.arange(1,JMAX):
                dv_dy[j,i]=(v[j,i] - v[j - 1,i]) / DY

        dv_dy[0,:]=(v[1,:] - v[0,:]) / DY

    else:
        if (call_case=='Correct_F'):
            for i in np.arange(0,IMAX):
                for j in np.arange(0,JMAX - 1):
                    dv_dy[j,i]=(v[j + 1,i] - v[j,i]) / DY

            dv_dy[JMAX-1,:]=(v[JMAX-1,:] - v[JMAX - 2,:]) / DY

        else:
            print('Undefined call case.')
    
# To compute du_dx:
    for i in np.arange(1,IMAX - 1):
        for j in np.arange(0,JMAX):
            du_dx[j,i]=(u[j,i + 1] - u[j,i - 1]) / (np.dot(2,DX))
    
    du_dx[:,0]=(u[:,1] - u[:,0]) / DX
    du_dx[:,IMAX-1]=(u[:,IMAX-1] - u[:,IMAX - 2]) / DX
    
# Finally, from the normal stress definition
# (assuming a Newtonian fluid and the Stokes' hypothesis) we have:
    tau_yy=np.dot(lambda_,(du_dx + dv_dy)) + np.dot(np.dot(2,mu),dv_dy)

    return tau_yy