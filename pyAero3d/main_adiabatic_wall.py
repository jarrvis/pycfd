from BC import BC
from DYNVIS import DYNVIS
from CONVER import CONVER
from THERMC import THERMC
from Primitive2E import Primitive2E
from Primitive2F import Primitive2F
from U2Primitive import U2Primitive

import numpy as np
import math


#############
# Grid size #
#############
    
IMAX=70
JMAX=70
MAXIT=10000.0
t=1
time=0

########################################
# Freestream conditions / Plate length #
########################################

# ISA sea level conditions
M_inf=4
a_inf=340.28
p_inf=101325
T_inf=288.16
LHORI=1e-05

########################
# Constants definition #
########################

gamma=1.4
mu_0=1.7894e-05
T_0=288.16
Pr=0.71
R=287

# Compute mu_inf by means of Sutherland's law by using the DYNVIS function
# mu_inf = DYNVIS(T_inf, mu_0, T_0); # Freestream dynamic viscosity [kg/(m*s)]

# Continue with the definition of other necessary variables
c_v=R / (gamma - 1)
c_p=gamma * c_v
rho_inf=p_inf / (R * T_inf)
Re_L=rho_inf*M_inf*a_inf*LHORI/DYNVIS(T_inf, mu_0, T_0)

# e_inf = c_v*T_inf; # Freestream specific internal energy [J/kg]
# Compute k_inf by means of the constant Prandtl number assumption
# k_inf = THERMC(Pr, c_p, mu_inf); # Freestream thermal conductivity [W/(m*K)]

# Continue with the definition of other necessary variables
delta=5*LHORI / math.sqrt(Re_L)

# Blasius calculation at the trailind edge [m]
LVERT=5* delta

# Calculate step sizes in x and y
DX=LHORI / (IMAX - 1)
DY=LVERT / (JMAX - 1)
x=np.arange(0,LHORI,DX)
y=np.arange(0,LVERT,DY)
CFL=0.5

#######################################
# Flow field variables initialization #
#######################################

# Only those variables that are needed in the calculation of the vectors U, E and F are initialized.
# The remaining flow properties can be obtained after as a postprocess to the solution.
p=np.full((JMAX,IMAX), p_inf)
rho=np.full((JMAX,IMAX), rho_inf)
T=np.full((JMAX,IMAX), T_inf)
u=np.full((JMAX,IMAX), M_inf * a_inf)

u[1,:]=0

v=np.zeros((JMAX,IMAX))
mu=DYNVIS(T,mu_0,T_0)
lambda_=-2/3 * mu
k=THERMC(Pr,c_p,mu)

# Other variables needed for intermediate calculations are defined here:
U1_p=np.zeros((JMAX,IMAX))
U2_p=np.zeros((JMAX,IMAX))
U3_p=np.zeros((JMAX,IMAX))
U5_p=np.zeros((JMAX,IMAX))
rho_p=np.zeros((JMAX,IMAX))
u_p=np.zeros((JMAX,IMAX))
v_p=np.zeros((JMAX,IMAX))
T_p=np.zeros((JMAX,IMAX))
p_p=np.zeros((JMAX,IMAX))

# Start the main loop
converged=False
rho_old=np.copy(rho)
delta_t = np.zeros((int(MAXIT)))

while (not converged and t <= MAXIT):

# Compute the appropriate value of the time step needed to satisfy the CFL stability criterion
    v_prime=np.max(np.max(np.dot(np.dot(4 / 3,mu[1:JMAX,1:IMAX] ** 2),gamma) / (np.dot(Pr,rho[1:JMAX,1:IMAX]))))
    delta_t_CFL=1.0 / (abs(u[1:JMAX,1:IMAX]) / DX + abs(v[1:JMAX,1:IMAX]) / DY + np.dot(np.sqrt(np.dot(np.dot(gamma,R),T[1:JMAX,1:IMAX])),np.sqrt(1 / DX ** 2 + 1 / DY ** 2)) + np.dot(np.dot(2,v_prime),(1 / DX ** 2 + 1 / DY ** 2)))
    delta_t[t]=np.dot(CFL,np.min(np.min(delta_t_CFL)))

# The first stage is to compute the solution vector U from the primitive flow field variables
    U1=np.copy(rho)
    U2=np.multiply(rho,u)
    U3=np.multiply(rho,v)
    U5=np.multiply(rho,(np.dot(c_v,T) + (u ** 2 + v ** 2) / 2))

##################
# Predictor step #
##################
# Calculate the flux vectors E (x derivatives) and F (y derivatives) from the primitive variables
    E1,E2,E3,E5 = Primitive2E(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,'Predict_E')
    F1,F2,F3,F5 = Primitive2F(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,'Predict_F')

# For interior points only:
# Forward differences
    for i in np.arange(1,IMAX-1):
        for j in np.arange(1,JMAX-1):
            U1_p[j,i] = U1[j,i] - np.dot(delta_t[t],((E1[j,i + 1] - E1[j,i])
                                                     / DX + (F1[j + 1,i] - F1[j,i]) / DY))
            U2_p[j,i] = U2[j,i] - np.dot(delta_t[t],((E2[j,i + 1] - E2[j,i])
                                                     / DX + (F2[j + 1,i] - F2[j,i]) / DY))
            U3_p[j,i] = U3[j,i] - np.dot(delta_t[t],((E3[j,i + 1] - E3[j,i])
                                                     / DX + (F3[j + 1,i] - F3[j,i]) / DY))
            U5_p[j,i] = U5[j,i] - np.dot(delta_t[t],((E5[j,i + 1] - E5[j,i])
                                                     / DX + (F5[j + 1,i] - F5[j,i]) / DY))

# Now, decode the flow field variables that will be needed for the calculation of predicted values
# of the flux vectors E and F.
    rho_p,u_p,v_p,T_p = U2Primitive(U1_p,U2_p,U3_p,U5_p,c_v)
    p_p = np.multiply(np.dot(rho_p,R),T_p)
    rho_p,u_p,v_p,p_p,T_p = BC(rho_p,u_p,v_p,p_p,T_p,rho_inf,np.dot(M_inf,a_inf),p_inf,T_inf,[],R,True)
    mu_p = DYNVIS(T_p,mu_0,T_0)
    lambda_p = np.dot(- 2 / 3,mu_p)
    k_p = THERMC(Pr,c_p,mu_p)

##################
# Corrector step #
##################
# Calculate the flux vectors E (x derivatives) and F (y derivatives) from the primitive variables.
    E1_p,E2_p,E3_p,E5_p = Primitive2E(rho_p,u_p,p_p,v_p,T_p,mu_p,lambda_p,k_p,c_v,DX,DY,'Correct_E')
    F1_p,F2_p,F3_p,F5_p = Primitive2F(rho_p,u_p,p_p,v_p,T_p,mu_p,lambda_p,k_p,c_v,DX,DY,'Correct_F')

# For interior points only:
# Rearward differences
    for i in np.arange(1,IMAX - 1):
        for j in np.arange(1,JMAX - 1):
            U1[j,i] = np.dot(1 / 2,(U1[j,i] + U1_p[j,i]
                                    - np.dot(delta_t[t],((E1_p[j,i] - E1_p[j,i - 1])
                                                         / DX + (F1_p[j,i] - F1_p[j - 1,i]) / DY))))
            U2[j,i] = np.dot(1 / 2,(U2[j,i] + U2_p[j,i]
                                    - np.dot(delta_t[t],((E2_p[j,i] - E2_p[j,i - 1])
                                                         / DX + (F2_p[j,i] - F2_p[j - 1,i]) / DY))))
            U3[j,i] = np.dot(1 / 2,(U3[j,i] + U3_p[j,i]
                                    - np.dot(delta_t[t],((E3_p[j,i] - E3_p[j,i - 1])
                                                         / DX + (F3_p[j,i] - F3_p[j - 1,i]) / DY))))
            U5[j,i] = np.dot(1 / 2,(U5[j,i] + U5_p[j,i]
                                    - np.dot(delta_t[t],((E5_p[j,i] - E5_p[j,i - 1])
                                                         / DX + (F5_p[j,i] - F5_p[j - 1,i]) / DY))))

# Finally, decode the corrected flow field variables.
    rho,u,v,T =U2Primitive(U1,U2,U3,U5,c_v)
    p = np.multiply(np.dot(rho,R),T)
    rho,u,v,p,T=BC(rho,u,v,p,T,rho_inf,np.dot(M_inf,a_inf),p_inf,T_inf,[],R,True)
    mu=DYNVIS(T,mu_0,T_0)
    lambda_=np.dot(- 2 / 3,mu)
    k=THERMC(Pr,c_p,mu)

####################
# Convergence check #
#####################
    converged=CONVER(rho_old,rho)
    rho_old=np.copy(rho)
    time=time + delta_t[t]
    t=t + 1
    print(t)

if (converged):
    continuity=np.dot(rho,u,rho_inf,np.dot(M_inf,a_inf),LVERT,DY)
else:
    error('CALCULATION FAILED: the maximum number of iterations has been reached before achieving convergence.')

if (continuity):
    disp('The calculation for M = 4 with an adiabatic wall boundary condition has finished successfully.')
else:
    error('The solution has converged to an invalid result.')

M=sqrt(u ** 2 + v ** 2) / sqrt(np.dot(np.dot(gamma,R),T))

