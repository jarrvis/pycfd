import numpy as np

def U2Primitive(U1,U2,U3,U5,c_v):


# Calculate (decode) the primitive flow field variables involved in the definition
# of the solutions vector U.
    
    rho=np.copy(U1)
    u=U2 / U1
    v=U3 / U1
    T=(U5 / U1 - ((U2 / U1) ** 2 + (U3 / U1) ** 2) / 2) / c_v

    return  rho, u, v, T