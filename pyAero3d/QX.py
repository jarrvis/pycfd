
import numpy as np
    

def QX(T,k,DX,call_case):

# Calculate the x-component of the heat flux vector.
    
    JMAX,IMAX=T.shape
    dT_dx=np.zeros((JMAX,IMAX))
# Calculate the derivative of temperature wrt x:
# For interior points we have two cases. At the boundaries there is only one possibility: a forward difference
# when i = 1 and a rearward difference when i = IMAX
    if (call_case=='Predict_E'):
        for i in np.arange(1,IMAX):
            for j in np.arange(0,JMAX):
                dT_dx[j,i]=(T[j,i] - T[j,i - 1]) / DX

        dT_dx[:,0]=(T[:,1] - T[:,0]) / DX
    else:
        if (call_case=='Correct_E'):
            for i in np.arange(0,IMAX - 1):
                for j in np.arange(0,JMAX):
                    dT_dx[j,i]=(T[j,i + 1] - T[j,i]) / DX
            dT_dx[:,IMAX-1]=(T[:,IMAX-1] - T[:,IMAX - 2]) / DX
        else:
            print('Undefined call case.')
    
# Then, following Fourier's law for heat conduction we have:
    q_x=np.dot(- k,dT_dx)
    return q_x