
import numpy as np

def TAUXX(u,v,lambda_,mu,DX,DY,call_case):

# Calculate the x-direction of the normal stress.
    
    JMAX,IMAX=u.shape

    du_dx=np.zeros((JMAX,IMAX))

    dv_dy=np.zeros((JMAX,IMAX))

# Calculate the derivative of u wrt x and the derivative of v wrt y (divergence of 2D velocity):
# For interior points we have two cases for the derivative of u wrt x and a unique case for the derivative of v wrt y.
# At the boundaries there is only one possibility: a forward difference when i,j = 1 and a rearward difference when i,j = IMAX,JMAX.
# To compute du_dx:
    if (call_case=='Predict_E'):
        for i in np.arange(1,IMAX):
            for j in np.arange(0,JMAX):
                du_dx[j,i]=(u[j,i] - u[j,i - 1]) / DX

        du_dx[:,0]=(u[:,1] - u[:,0]) / DX

    else:
        if (call_case=='Correct_E'):
            for i in np.arange(0,IMAX - 1):
                for j in np.arange(0,JMAX):
                    du_dx[j,i]=(u[j,i + 1] - u[j,i]) / DX

            du_dx[:,IMAX-1]=(u[:,IMAX-1] - u[:,IMAX-2]) / DX

        else:
            print('Undefined call case.')
    
# To compute dv_dy:
    for i in np.arange(0,IMAX):
        for j in np.arange(1,JMAX - 1):
            dv_dy[j,i]=(v[j + 1,i] - v[j - 1,i]) / 2*DY

    
    dv_dy[0,:]=(v[1,:] - v[0,:]) / DY

    
    dv_dy[JMAX-1,:]=(v[JMAX-1,:] - v[JMAX-2,:]) / DY

    
# Finally, from the normal stress definition (assuming a Newtonian fluid and the Stokes' hypothesis) we have:
    tau_xx=np.dot(lambda_,(du_dx + dv_dy)) + np.dot(np.dot(2,mu),du_dx)
    return tau_xx
