
def DYNVIS(T, mu_0, T_0):

# Calculate dynamic viscosity for calorically perfect air by means of
# Sutherland's law.
    
    mu= mu_0 * (T / T_0) ** (3 / 2) * (T_0 + 110) / (T + 110)
    return mu