import numpy as np
from TAUYY import TAUYY
from TAUXY import TAUXY
from QY import QY
    
def Primitive2F(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,call_case):

# Calculate the flux vector F from the primitive flow field variables.
    
    F1=np.dot(rho,v)
    
# For F2 we need the yx component of the shear stress (tau_yx)
    tau_yx=TAUXY(u,v,mu,DX,DY,call_case)
    
    F2=np.dot(np.dot(rho,u),v) - tau_yx
    # To compute F3 first we need to calculate the y-direction of the normal stress (tau_yy)
    tau_yy=TAUYY(u,v,lambda_,mu,DX,DY,call_case)
    F3=np.dot(rho,v ** 2) + p - tau_yy
    
# Finally, for F5 we also need the y-component of the heat flux (q_y)
    q_y=QY(T,k,DY,call_case)
    F5=np.dot((np.dot(rho,(np.dot(c_v,T) + (u ** 2 + v ** 2) / 2)) + p),v) - np.dot(u,tau_yx) - np.dot(v,tau_yy) + q_y

    return F1,F2,F3,F5